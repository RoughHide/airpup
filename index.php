<?php
    require 'lib/functions.php';
    $pets = get_pets();

    $pets = array_reverse($pets);

    $cleverWelcomemessage = 'All the love, none of the crap!';
    $pupCount = count($pets);
?>
<?php require 'layout/header.php'; ?>

    <div class="jumbotron">
        <div class="container">
            <h1><?php echo strtoupper(strtolower($cleverWelcomemessage)); ?></h1>

            <p>Over <?php echo $pupCount ?> pet friends!</p>

            <p><a class="btn btn-primary btn-lg">Learn more &raquo;</a></p>
        </div>
    </div>


    <div class="container">
        <div class="row">
            <?php foreach ($pets as $cutepet) { ?>
                <div class="col-md-4 pet-list-item">
                    <h2><?php echo $cutepet['name']; ?></h2>

                    <img src="/images/<?php echo $cutepet['filename']; ?>" class="img-rounded" />

                    <blockquote class="pet-details">
                        <span class="label label-info"><?php echo $cutepet['breed']; ?> </span>
                        <?php
                            if (!array_key_exists('age', $cutepet) || $cutepet['age'] == '') {
                                echo 'Unkown';
                            } elseif ($cutepet['age'] == 'hidden') {
                                echo '(contact owner for age)';
                            } else {
                                echo $cutepet['age'];
                            }
                        ?>
                        <?php echo $cutepet['weight']; ?> lbs
                    </blockquote>

                    <p>
                        <?php echo $cutepet['bio']; ?>
                    </p>
                </div>
            <?php } ?>
        </div>

        <hr>

<?php require 'layout/footer.php'; ?>

